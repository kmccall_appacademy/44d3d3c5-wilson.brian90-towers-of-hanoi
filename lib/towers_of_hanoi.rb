# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.


class TowersOfHanoi
  attr_accessor :towers

  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def move(a, b)
    a = a.to_i
    b = b.to_i
    @towers[b] << @towers[a].pop
  end

  def valid_move?(a, b)
    a = a.to_i
    b = b.to_i
    if a < 0 || a > 2
      return false
    end

    if b < 0 || b > 2
      return false
    end

    if @towers[a].empty?
      return false
    end

    if @towers[b].empty?
      return true
    end

    if @towers[a].last < @towers[b].last
      return true
    end

    return false
  end

  def play
    while !won? do
      puts "Move from:"
      from = gets.chomp
      puts "Move to:"
      to = gets.chomp

      if valid_move?(from, to)
        move(from, to)
      else
        puts "bad move"
      end

      render

    end
  end

  def render
    puts @towers.to_s
  end

  def won?
    if @towers[2] == [3,2,1] || @towers[1] == [3,2,1]
      return true
    else
      return false
    end
  end

end
